﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly IEntityUpdater<T> _entityUpdater;

        private List<T> Data { get; set; } = new List<T>();

        public InMemoryRepository(IDataFactory<T> dataFactory, IEntityUpdater<T> entityUpdater)
        {
            _entityUpdater = entityUpdater;
            Data.AddRange(dataFactory.Generate());
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            var readonlyData = Data.ToArray();
            return Task.FromResult<IEnumerable<T>>(readonlyData);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public async Task AddAsync(T entity)
        {
            var item = await GetByIdAsync(entity.Id);
            if (item != null)
                throw new ArgumentNullException($"Item with id {entity.Id} already exists");
            Data.Add(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            var item = await GetByIdAsync(entity.Id) ?? throw new ArgumentNullException($"Item with id {entity.Id} not found");
            await _entityUpdater.Update(item, entity);
        }

        public async Task DeleteAsync(Guid entityId)
        {
            var item = await GetByIdAsync(entityId) ?? throw new ArgumentNullException($"Item with id {entityId} not found");
            Data.Remove(item);
        }
    }
}