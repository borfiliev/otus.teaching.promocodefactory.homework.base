﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    public class DefaultUpdater<T> : IEntityUpdater<T> where T : BaseEntity
    {
        public Task Update(T updatedEntity, T updateFrom)
        {
            var typeProperties = typeof(T)
                   .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                   .Where(x => x.SetMethod != null)
                   .Where(x => x.GetMethod != null);
            foreach (var property in typeProperties)
            {
                var value = property.GetValue(updateFrom);
                property.SetValue(updatedEntity, value);
            }
            return Task.CompletedTask;
        }
    }
}
