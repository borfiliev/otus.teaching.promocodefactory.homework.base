﻿using System;
using System.Linq;
using System.Reflection;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}