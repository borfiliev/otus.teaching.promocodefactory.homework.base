﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IEntityUpdater<T> where T:BaseEntity
    {
        Task Update(T updatedEntity, T updateFrom);
    }
}
